---
layout: comparison_page
title: GitLab compared to other tools
suppress_header: true
image_title: '/images/comparison/title_image.png'
extra_css:
  - compared.css
---

## GitLab CE/EE vs. SaaS

### Containment
Containing your company's IP behind the company's firewall gives you protection from unauthorized access. Learn from the lessons of Code Space:

[Lessons from Code Space](http://www.infoworld.com/article/2608076/data-center/murder-in-the-amazon-cloud.html)

### Integrations
Integrating with Authentication and Authorization (LDAP / AD), issue tracking, CI, deployment and other tools such as ALM, PLM, Agile and Automation tools.

### Control
Take control of maintenance downtime, don't be at the mercy of your hosting provider. Control how and where your code is backed up and stored.

### Choice and Flexibility
GitLab CE/EE can be installed on physical servers, virtualized servers (dedicated or shared), purpose-built appliances and virtualized appliances. These aren’t available with hosted solutions. Likewise, most on-premises servers can be deployed on a variety of operating systems and there’s more choice of on-premises solutions in general.

### Retrieval
Getting your IP back from cloud vendors that store data in proprietary formats can be a costly and lengthy process. No such trouble with GitLab CE/EE.

### In good company
GitLab is the most adopted on-premises solution for developer collaboration, deployed at over 100,000 organizations worldwide.
